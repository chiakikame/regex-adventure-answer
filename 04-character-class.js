module.exports = function(input) {
  return /^[aeiou0-9]/.test(input);
};
