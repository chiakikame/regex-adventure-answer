module.exports = function(input) {
  return /^[^0-9][^A-Z]/.test(input);
};
