module.exports = function(input) {
  return /^\d+\.jpe?g$/.test(input);
};
