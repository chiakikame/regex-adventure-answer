module.exports = function(str) {
  let match = /\bx=(\d+)\b/.exec(str);
  return match ? match[1] : null;
}
