module.exports = function(str) {
  let match = /x=(\d+)/.exec(str);
  return match ? match[1] : null;
}
